require 'game'
require 'rspec'

describe Game do
  
  let(:game) { Game.new }
  
  let(:riley) { double("Riley", :hand => nil) }
  let(:lb) { double("Little Bear", :hand => nil) }
  let(:ozzy) { double("Ozzy", :hand => nil) }
  
  context "initialize" do
    
    it "should begin with no players" do
      expect(game.players.count).to eq(0)
    end
    
    it "should instantiate a new deck object" do
      expect(game.deck.cards.count).to eq(52)
    end
    
    it "should begin with an empty pot" do
      expect(game.pot).to eq(0)
    end
  
  end
  
  context "players" do
    
    it "should add players" do
      game.add_players(riley, lb, ozzy)
      
      expect(game.players).to eq([riley, lb, ozzy])
    end
  end
  
  context "game loop" do
  
    it "should shift the betting order after each hand" do
      game.add_players(riley, lb, ozzy)
      game.shift_betting_order
      expect(game.players).to eq([lb, ozzy, riley])
    end
    
    it "should choose the winning hand" do
      expect(riley.hand).to receive(:beats?).with(lb.hand)
      game.choose_winner
    end
    
    it "should end the game if only one player has a bankroll left" do
    end
  
  end
end