require 'deck'
require 'rspec'

describe Deck do
  subject(:deck) { Deck.new }

  it "initializes to the right size" do
    expect(deck.cards.size).to eq(52)
  end

  it "contains all unique cards" do
    expect(deck.cards.uniq).to eq(deck.cards)
  end

  it "passes out right number of cards" do
    deck.take(5)
    expect(deck.cards.size).to eq(47)
  end

  it "passes out cards from top of deck" do
    top_five = deck.cards[-5..-1]

    expect(deck.take(5)).to eq(top_five)
  end

  it "returns cards back to bottom of deck" do
    hand = deck.take(5)
    deck.return(hand)
    expect(deck.cards[0..4]).to eq(hand)
  end

end