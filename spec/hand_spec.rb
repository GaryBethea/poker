require 'hand'
require 'rspec'

describe Hand do

  let(:deck) { double("deck") }

  context "initializing" do
    it "should draw five cards" do
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :deuce),
        Card.new(:diamonds, :ace)
      ]

      allow(deck).to receive(:take).with(5).and_return(deck_cards)

      #deck = double("deck")
      #allow(deck).to receive(:take).with(5).and_return(deck_cards)

      hand = Hand.new(deck)

      expect(hand.cards).to eq(deck_cards)
    end
  end
  
  context "draws and discards cards correctly" do
    
    let(:original_deck) do [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :deuce),
        Card.new(:diamonds, :ace)
      ] 
    end

    it "increases hand size by number of cards drawn" do
      
      deck = double("deck")
      
      deck_cards = [
        Card.new(:diamonds, :eight),
        Card.new(:diamonds, :nine),
        Card.new(:diamonds, :ten)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(original_deck)
      allow(deck).to receive(:take).with(3).and_return(deck_cards)
      hand = Hand.new(deck)

      hand.cards = [Card.new(:diamonds, :six),
        Card.new(:diamonds, :seven)]
        
      hand.draw(3)
      expect(hand.cards.size).to eq(5)
    end

    it "decreases hand size by number of cards discarded" do
      
      deck = double("deck")
      
      deck_cards = [
        Card.new(:diamonds, :six),
        Card.new(:diamonds, :seven),
        Card.new(:diamonds, :eight),
        Card.new(:diamonds, :nine),
        Card.new(:diamonds, :ten)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      allow(deck).to receive(:return).with([hand.cards[1], hand.cards[4]])
      hand = Hand.new(deck)
      
      expected_remaining = [hand.cards[0], hand.cards[2], hand.cards[3]]
      hand.discard([1, 4])
      expect(hand.cards).to eq(expected_remaining)
    end
  end
  
  # context "rendering" do
#
#     it "renders the hand for the player to see" do
#       #stub
#     end
#   end

  context "identifying sets" do
    
    # let(:original_deck) do [
#         Card.new(:diamonds, :four),
#         Card.new(:clubs, :ten),
#         Card.new(:diamonds, :jack),
#         Card.new(:hearts, :deuce),
#         Card.new(:diamonds, :ace)
#       ]
#     end

    it "identifies high card" do
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :deuce),
        Card.new(:diamonds, :ace)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(0)
    end

    it "identifies a single pair" do
      
      # hand = Hand.new(deck)
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :jack),
        Card.new(:diamonds, :ace)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(1)
    end

    it "identifies two pairs" do
      # allow(deck).to receive(:take).with(5)
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :four),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :jack),
        Card.new(:diamonds, :ace)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(2)
    end

    it "identifies three of a kind" do
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :four),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :four),
        Card.new(:diamonds, :ace)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(3)

    end

    it "identifies straight" do
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :five),
        Card.new(:diamonds, :six),
        Card.new(:hearts, :seven),
        Card.new(:diamonds, :eight)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(4)
    end

    it "identifies flush" do
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:diamonds, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:diamonds, :deuce),
        Card.new(:diamonds, :ace)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(5)
    end

    it "identifies full house" do
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :four),
        Card.new(:spades, :four),
        Card.new(:hearts, :deuce),
        Card.new(:diamonds, :deuce)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(6)
    end

    it "identifies four of a kind" do
      
      deck_cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :four),
        Card.new(:spades, :four),
        Card.new(:hearts, :four),
        Card.new(:diamonds, :ace)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(7)
    end

    it "identifies straight flush (mother fucker!!!)" do
      
      deck_cards = [
        Card.new(:diamonds, :six),
        Card.new(:diamonds, :seven),
        Card.new(:diamonds, :eight),
        Card.new(:diamonds, :nine),
        Card.new(:diamonds, :ten)
      ]
      
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      hand = Hand.new(deck)
      
      expect(hand.find_best_set).to eq(8)
    end
  end

  context "comparing hands" do

    it "picks best set in clear battle" do
      allow(deck).to receive(:take).with(5).and_return(deck_cards)

      hand1 = Hand.new(deck)
      hand1.cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :four),
        Card.new(:spades, :four),
        Card.new(:hearts, :four),
        Card.new(:diamonds, :ace)
      ]

      hand2 = Hand.new(deck)
      hand2.cards = [
        Card.new(:diamonds, :six),
        Card.new(:diamonds, :seven),
        Card.new(:diamonds, :eight),
        Card.new(:diamonds, :nine),
        Card.new(:diamonds, :ten)
      ]

      expect(hand1.beats?(hand2)).to eq(false)
    end

    it "picks best hand when sets are tied" do
      allow(deck).to receive(:take).with(5).and_return(deck_cards)
      
      hand1 = Hand.new(deck)
      hand1.cards = [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :jack),
        Card.new(:diamonds, :ace)
      ]

      hand2 = Hand.new(deck)
      hand2.cards = [
        Card.new(:hearts, :four),
        Card.new(:diamonds, :ten),
        Card.new(:clubs, :jack),
        Card.new(:spades, :jack),
        Card.new(:diamonds, :king)
      ]

      expect(hand1.beats?(hand2)).to eq(true)
    end


  end

end