require 'player'
require 'rspec'

describe Player do
  
  context "attributes" do
    
    let(:example_deck) { [
        Card.new(:diamonds, :four),
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :deuce),
        Card.new(:diamonds, :ace)
      ] }
    let(:deck) { double("deck") }
    subject(:player) { Player.new("Gary", 1000, deck) }
    
    it "should create a bankroll from argument" do
      expect(player.bankroll).to eq(1000)
    end
    
    it "should be passed a deck to play with" do
      expect(player.deck).to eq(deck)
    end
    
    it "should be able to be dealt a new hand of 5 cards" do
      allow(deck).to receive(:take).with(5).and_return(example_deck)
      
      player.deal
      expect(player.hand.cards.size).to eq(5)
    end
    
  end
  
  context "lets player control their hand" do
    
    let(:example_deck) { [
        Card.new(:diamonds, :four), # 0
        Card.new(:clubs, :ten),
        Card.new(:diamonds, :jack),
        Card.new(:hearts, :deuce),
        Card.new(:diamonds, :ace), # 4
      ] }
    let(:new_cards) { [Card.new(:hearts, :three), Card.new(:spades, :five)] }
    let(:deck) { double("deck") }
    subject(:player) { Player.new("Gary", 1000, deck) }
    let(:gets) { "1, 4" }
    
    it "lets players choose which cards to discard" do
      allow(deck).to receive(:take).with(5).and_return(example_deck)
      
      player.deal
      
      return1 = player.hand.cards[0]
      return2 = player.hand.cards[4]
      
      allow(deck).to receive(:return).with([return1, return2])
      allow(deck).to receive(:take).with(2).and_return(new_cards)
      
      player.pick_discards(:log_stub)
      
      expect(player.hand).to receive(:discard).with([0, 4])
    end
    
  end
  
  context "betting" do
      
      let(:deck) { double("deck") }
      
      let(:player1) { Player.new("Gary", 100, deck) }
      
      let(:player2) { Player.new("Kate", 200, deck) }
    
    it "subtracts the bet amount from a player's bankroll" do
      player1.take_bet(10, 50)
      expect(player1.bankroll).to eq(90)
    end
    
    it "adds the bet amount to the game pot" do
      expect(player1.take_bet(10, 50)).to eq(60)
    end
    
    it "can tell whether a player has folded" do
      player1.fold
      expect(player1.still_in?).to eq(false)
    end
    
    it "pays the pot to the winning player" do
      player2.pay_winnings(500)
      expect(player2.bankroll).to eq(700)
    end
    
    let(:gets) { "15" }
    
    it "allows player to raise the current bet" do
      expect(player1).to receive(:take_bet).with(15, 100)
      player1.raise_bet(10, 100)
    end
    
    let(:gets) { "9_000" }
    
    it "stops players from 'raising' the bet to a lower amount" do
      expect { player1.raise_bet(10, 100) }.to 
      raise_error("You don't have that kind of dough!")
    end
    
    let(:gets) { "9" }
    
    it "stops players from betting more than they have" do
      expect { player1.raise_bet(10, 100) }.to 
      raise_error("That's not a raise! Need more cash.")
    end
    
  end
  
end

