require 'card'
require 'rspec'

describe Card do

  describe "should initialize with a value and a suit" do
    subject(:card) { Card.new(:hearts, :king) }

    it "should have value" do
      expect(card.value).to eq(:king)
    end

    it "should have suit" do
      expect(card.suit).to eq(:hearts)
    end

  end
end