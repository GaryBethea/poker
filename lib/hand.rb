require_relative "deck"

class Hand
  
SETS = [:high_card, :pair, :two_pairs, :three_of_a_kind, :straight, 
        :flush, :full_house, :four_of_a_kind, :straight_flush]

  attr_accessor :cards
  attr_reader :deck

  def initialize(deck)
    @deck = deck
    @cards = []
    draw(5)
  end

  def find_best_set
    SETS.reverse.each do |set|
      return SETS.index(set) if method(set).call
    end
  end
  
  def beats?(other_hand)
    # first, find out if we have a better set than other_hand
    case find_best_set <=> other_hand.find_best_set
    when -1
      false
    when 0
      # if tie, compare strength of the overall hand, sorted
      sort_cards > other_hand.sort_cards
    when 1
      true
    end
  end

  def draw(num)
    @cards += self.deck.take(num)
  end

  def discard(card_pos)
    discards = []
    card_pos.each do |pos|
      discards << self.cards[pos]
      self.cards.delete_at(pos)
    end
    
    self.deck.return(discards)
  end
  
  def render
    self.cards.each { |card| puts card.to_s }
  end
  
  # eventually start private methods here
  
  def sort_cards
    # sets up hand so that in the case of a set tie, the game goes to the
    # strongest hand, as per poker rules
    
    counts = count_dups
    counts = counts.to_a
    
    repeated = counts.select { |val_freq| val_freq[1] > 1  }
    not_repeated = counts.reject { |val_freq| val_freq[1] > 1  }
    
    sorted = []
    
    # for repeated values, push most requent values to front of array first
    repeated.sort! { |val_freq1, val_freq2 | val_freq2[1] <=> val_freq1[1] }
    
    repeated.each do |val_freq|
      val_freq.last.times { sorted << val_freq.first }
    end
    
    # sort non-repeated values largest first and push to sorted array
    not_repeated.sort! { |one, two| two <=> one }
    
    not_repeated.each do |val_freq|
      sorted << val_freq.first
    end
    
    sorted
  end
  
  def count_dups
    counts = Hash.new(0)
    
    self.cards.each do |card|
      poker_value = card.poker_value
      counts[poker_value] += 1
    end
    
    counts
  end
  
  def high_card
    true
  end
  
  def pair
    count_dups.any? { |val, freq| freq == 2  }
  end
  
  def two_pairs
    counter = 0
    count_dups.each_value do |freq|
      counter += 1 if freq > 1
    end
    
    counter > 1
  end
  
  def three_of_a_kind
    count_dups.any? { |val, freq| freq == 3  }
  end
  
  def straight
    all_straight = true
    
    vals = self.cards.map { |card| card.poker_value }
    
    (0..vals.count - 2).each do |index|
      this_val = vals[index]
      next_val = vals[index + 1]
      diff = this_val - next_val
      
      all_straight = false unless diff == 1
    end
    
    all_straight # || ace_low_straight
  end
  
  # def ace_low_straight
  
  def flush
    self.cards.all? do |card|
      suit = card.suit
      suit == :clubs || suit == :diamonds || suit == :spades || suit == :hearts
    end
  end
  
  def full_house
    three_of_a_kind && pair
  end
  
  def four_of_a_kind
    count_dups.any? { |val, freq| freq == 4  }
  end
  
  def straight_flush
    straight && flush
  end
end

# h = Hand.new(Deck.new)
#
# h.cards = [
#   Card.new(:diamonds, :four),
#   Card.new(:clubs, :four),
#   Card.new(:diamonds, :jack),
#   Card.new(:hearts, :jack),
#   Card.new(:clubs, :jack)
# ]
#
# h.render