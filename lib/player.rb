require_relative 'hand'

class Player
  
  attr_reader :name, :bankroll, :hand, :deck, :game
  
  def initialize(name, bankroll, game)
    @name = name
    @bankroll = bankroll
    @game = game
    @deck = game.deck
    @hand = nil
    @still_in = true
    @raised = false
  end
  
  def deal
    @hand = Hand.new(self.deck)
  end
  
  def pick_discards
    puts game.log
    self.hand.render
    
    puts "Would you like to discard any cards? y/n"
    y_n = gets.chomp
    return if y_n.include?("n")
    
    puts "Write the index of any cards you would like to discard:"
    discards = gets.chomp.split(", ").map { |index| Integer(index) }
    
    self.hand.discard(discards)
    self.hand.draw(discards.count)
    self.hand.render
    
    "#{name} discarded #{discards.count} cards"
  end # return string for game log
  
  def pay_winnings
    @bankroll += game.pot
    game.pot = 0
  end
  
  def manage_bet
    puts game.log
    puts "#{name}, the pot is currently $#{game.pot} and the bet to see is $#{game.current_bet}"
    self.hand.render
    puts "How would you like to bet? SEE, RAISE, or FOLD? s/r/f"
    
    action = gets.chomp
    case action
    when "s"
      @raised = false
      take_bet(game.current_bet)
      "#{name} saw"
    when "r"
      raise_bet(game.current_bet)
    when "f"
      @raised = false
      self.fold
      "#{name} folded"
    end
    
  end # return string for game log
  
  def take_bet(amt)
    @bankroll -= amt
    game.pot += amt
  end
  
  def raise_bet(starting_amt)
    puts "How much would you like to bet?"
    
    raise_amt = Integer(gets.chomp)
    raise "You don't have that kind of dough!" if raise_amt > self.bankroll
    
    if raise_amt <= starting_amt
      raise "That's not a raise! The current bet is #{game.current_bet}."
    end
    
    @raised = true
    game.current_bet = raise_amt
    take_bet(raise_amt)
    
    "#{name} raised the bet to #{raise_amt}"
  end
  
  def fold
    @still_in = false
  end
  
  def still_in?
    @still_in
  end
  
  def raised?
    @raised
  end
  
  def inspect
    "#{name}"
  end
end

# p = Player.new(1000, Deck.new)
#
# p.deal
#
# p.pick_discards
#
# p p.hand.render