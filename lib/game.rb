require_relative 'player'

class Game
  attr_reader :deck, :players, :log
  attr_accessor :current_bet, :pot
  
  def initialize
    @deck = Deck.new
    @pot = 0
    @players = []
    @log = []
    @current_bet = 0
  end
  
  def add_players(*players)
    players.each { |player| self.players << player }
  end
  
  def shift_betting_order
    shifted = self.players.shift
    self.players << shifted
  end
  
  def choose_winner(players)
    return players.first if players.count == 1
    
    survivors = []
    
    (players.count-2).each do |index|
      if players[index].beats?(player[index + 1])
        survivors << players[index] if players[index].still_in?
      end
    end
    
    choose_winner(survivors)
  end
  
  def play
    loop do # need to check when only one player has bankrole
      self.players.each { |player| player.deal }
      # no ante for now
    
      betting_round
    
      self.players.each { |player| player.pick_discards }
    
      betting_round
    
      winner = choose_winner(self.players)
      winner.pay_winnings
    end
  end
  
  def betting_round
    # better order must be changed based on raises, but we don't want to change
    # player order, which affects the next round of betting
    betters = self.players.dup 
    
    
    betters.each_with_index do |better, index|
      
      if better.still_in?
        self.log << better.manage_bet
        
        # if a better raised, the iteration must continue until every one has
        # seen the raise or folded
        # not behaving like I'd hoped
        betters << betters.shift(index) if better.raised? && index != 0
      end
    end
  end
  
end

g = Game.new
gary = Player.new("Gary", 100, g)
kate = Player.new("Kate", 200, g)

g.add_players(gary, kate)

# p g.players.count

g.play