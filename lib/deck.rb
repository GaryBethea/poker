require_relative "card"

class Deck
  attr_accessor :cards

  def initialize
    @cards = generate_deck
    shuffle
  end

  def take(num)
    self.cards.pop(num)
  end

  def return(return_cards)
    self.cards = return_cards + self.cards
  end

  def shuffle
    self.cards.shuffle!
  end

  private

  def generate_deck
    Card.suits.map do |suit|
      Card.values.map do |value|
        Card.new(suit, value)
      end
    end.flatten
  end

end